package adapter;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 01/10/16
 * Time: 7:17 PM
 */
public class MallardDuck implements Duck {

    @Override
    public void quack() {
        System.out.println("Quack");
    }

    @Override
    public void fly() {
        System.out.println("Fly");
    }
}
