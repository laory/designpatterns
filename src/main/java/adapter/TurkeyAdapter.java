package adapter;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 01/10/16
 * Time: 8:05 PM
 */
public class TurkeyAdapter implements Duck {

    private final Turkey turkey;

    public TurkeyAdapter(Turkey turkey) {
        this.turkey = turkey;
    }

    @Override
    public void quack() {
        turkey.gobble();
    }

    @Override
    public void fly() {
        turkey.fly();
    }
}
