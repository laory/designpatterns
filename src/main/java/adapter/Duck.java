package adapter;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 01/10/16
 * Time: 7:12 PM
 */
public interface Duck {

    void quack();
    void fly();
}
