package adapter;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 01/10/16
 * Time: 7:17 PM
 */
public class WildTurkey implements Turkey {

    @Override
    public void gobble() {
        System.out.println("Gobble");
    }

    @Override
    public void fly() {
        System.out.println("Fly");
    }
}
