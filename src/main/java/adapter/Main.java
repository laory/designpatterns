package adapter;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 01/10/16
 * Time: 8:08 PM
 */
public class Main {

    private static void tryDuck(Duck duck) {
        duck.quack();
        duck.fly();
    }

    private static void tryTurkey(Turkey turkey) {
        turkey.gobble();
        turkey.fly();
    }

    public static void main(String[] args) {
        WildTurkey turkey = new WildTurkey();
        MallardDuck duck = new MallardDuck();
        TurkeyAdapter turkeyAdapter = new TurkeyAdapter(turkey);
        DuckAdapter duckAdapter = new DuckAdapter(duck);
        tryDuck(duck);
        tryDuck(turkeyAdapter);
        System.out.println();
        tryTurkey(turkey);
        tryTurkey(duckAdapter);
    }
}
