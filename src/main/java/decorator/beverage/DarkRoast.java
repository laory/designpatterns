package decorator.beverage;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 20/9/16
 * Time: 10:52 PM
 */
public class DarkRoast extends Beverage {

    @Override
    public String getDescription() {
        return "Dark roast";
    }

    @Override
    public double cost() {
        return 2.5;
    }
}
