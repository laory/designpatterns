package decorator.beverage.condiment;

import decorator.beverage.Beverage;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 20/9/16
 * Time: 10:52 PM
 */
public class Milk extends Condiment {

    public Milk(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String getCondimentDescription() {
        return "Milk";
    }

    @Override
    public double getCondimentCost() {
        return 0.35;
    }
}
