package decorator.beverage.condiment;

import decorator.beverage.Beverage;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 20/9/16
 * Time: 10:50 PM
 */
public abstract class Condiment extends Beverage {

    private final Beverage beverage;

    Condiment(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public double cost() {
        return getCondimentCost() + beverage.cost();
    }

    @Override
    public String getDescription() {
        return String.format("%s with %s", beverage.getDescription(), getCondimentDescription());
    }

    public abstract String getCondimentDescription();

    public abstract double getCondimentCost();
}
