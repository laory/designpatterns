package decorator.beverage;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 20/9/16
 * Time: 10:48 PM
 */
public abstract class Beverage {

    public abstract String getDescription();

    public abstract double cost();

    @Override
    public String toString() {
        return "Beverage{"
               + "description=\"" + getDescription() + "\", "
               + "cost=" + cost()
               + "}";
    }
}
