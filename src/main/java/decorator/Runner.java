package decorator;

import decorator.beverage.Beverage;
import decorator.beverage.Espresso;
import decorator.beverage.condiment.Whip;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 20/9/16
 * Time: 11:15 PM
 */
public class Runner {

    public static void main(String[] args) {
        Beverage beverage = new Whip(new Whip(new Espresso()));
        System.out.println(beverage);
    }
}
