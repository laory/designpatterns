package state;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/5/2016
 * Time: 10:44 AM
 */
public class BubbleGumMachine {

    private       State       currentState = new NoCoinState(this);
    private final List<State> states       = new ArrayList<State>() {{ // the order of states maters
        add(currentState);
        add(new CoinHasBeenPutState(BubbleGumMachine.this));
        add(new SoldState(BubbleGumMachine.this));
    }};
    private int gumCount;

    public BubbleGumMachine(int gumCount) {
        this.gumCount = gumCount;
    }

    public void putCoin() {
        currentState.putCoin();
    }

    public void ejectCoin() {
        currentState.ejectCoin();
    }

    public void turnCrank() {
        currentState.turnCrank();
        currentState.dispense();
    }

    public boolean hasEnoughGums() {
        return gumCount > 0;
    }

    private void setAnotherState(State state, Function<Integer, Integer> indexCalculator) {
        int i = states.indexOf(state);
        if (i == -1)
            throw new IllegalStateException("State: " + state + " is not applicable for this machine");
        int newIndex = indexCalculator.apply(i);
        currentState = states.get(newIndex);
    }

    protected void setPrevState(State state) {
        setAnotherState(state, i -> i > 0 ? i - 1 : states.size() - 1);
    }

    protected void setNextState(State state) {
        setAnotherState(state, i -> (i + 1) % states.size());
    }

    protected void rollOutGum() {
        if (hasEnoughGums())
            gumCount--;
    }

    protected void setSoldOutState() {
        currentState = new SoldOutState();
    }

    public static void main(String[] args) {
        BubbleGumMachine bubbleGumMachine = new BubbleGumMachine(3);

        bubbleGumMachine.putCoin();
        bubbleGumMachine.putCoin();
        bubbleGumMachine.turnCrank();

        bubbleGumMachine.putCoin();
        bubbleGumMachine.ejectCoin();
        bubbleGumMachine.putCoin();
        bubbleGumMachine.turnCrank();
        bubbleGumMachine.turnCrank();

        bubbleGumMachine.putCoin();
        bubbleGumMachine.turnCrank();

        bubbleGumMachine.putCoin();
        bubbleGumMachine.turnCrank();
        bubbleGumMachine.ejectCoin();
    }

}

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/5/2016
 * Time: 10:43 AM
 */
class CoinHasBeenPutState implements State {

    private final BubbleGumMachine bubbleGumMachine;

    private final String name = "CoinHasBeenPutState";

    public CoinHasBeenPutState(BubbleGumMachine bubbleGumMachine) {
        this.bubbleGumMachine = bubbleGumMachine;
    }

    @Override
    public void putCoin() {
        System.out.println("Coin has already been put.");
    }

    @Override
    public void ejectCoin() {
        System.out.println("Coin has been ejected.");
        bubbleGumMachine.setPrevState(this);
    }

    @Override
    public void turnCrank() {
        System.out.println("Crank has been turned.");
        bubbleGumMachine.setNextState(this);
    }

    @Override
    public void dispense() {
        System.out.println("Turn the crank first.");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CoinHasBeenPutState that = (CoinHasBeenPutState) o;

        if (bubbleGumMachine != null ? !bubbleGumMachine.equals(that.bubbleGumMachine) : that.bubbleGumMachine != null)
            return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = bubbleGumMachine != null ? bubbleGumMachine.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CoinHasBeenPutState{" +
               "bubbleGumMachine=" + bubbleGumMachine +
               ", name='" + name + '\'' +
               '}';
    }
}

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/5/2016
 * Time: 10:43 AM
 */
class NoCoinState implements State {

    private final BubbleGumMachine bubbleGumMachine;

    private final String name = "NoCoinState";

    public NoCoinState(BubbleGumMachine bubbleGumMachine) {
        this.bubbleGumMachine = bubbleGumMachine;
    }

    @Override
    public void putCoin() {
        System.out.println("Coin has been put.");
        bubbleGumMachine.setNextState(this);
    }

    @Override
    public void ejectCoin() {
        System.out.println("There is no coin to eject.");
    }

    @Override
    public void turnCrank() {
        System.out.println("Put the coin first.");
    }

    @Override
    public void dispense() {
        System.out.println("You haven't put a coin.");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        NoCoinState that = (NoCoinState) o;

        if (bubbleGumMachine != null ? !bubbleGumMachine.equals(that.bubbleGumMachine) : that.bubbleGumMachine != null)
            return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = bubbleGumMachine != null ? bubbleGumMachine.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CoinHasBeenPutState{" +
               "bubbleGumMachine=" + bubbleGumMachine +
               ", name='" + name + '\'' +
               '}';
    }
}

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/5/2016
 * Time: 11:45 AM
 */
class SoldOutState implements State {

    @Override
    public void putCoin() {
        System.out.println("All gums have been sold out");
    }

    @Override
    public void ejectCoin() {
        System.out.println("All gums have been sold out");
    }

    @Override
    public void turnCrank() {
        System.out.println("All gums have been sold out");
    }

    @Override
    public void dispense() {
        System.out.println("All gums have been sold out");
    }
}

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/5/2016
 * Time: 10:43 AM
 */
class SoldState implements State {

    private final BubbleGumMachine bubbleGumMachine;

    private final String name = "SoldState";

    public SoldState(BubbleGumMachine bubbleGumMachine) {
        this.bubbleGumMachine = bubbleGumMachine;
    }

    @Override
    public void putCoin() {
        System.out.println("Dispense a gum first.");
    }

    @Override
    public void ejectCoin() {
        System.out.println("Dispense a gum first.");
    }

    @Override
    public void turnCrank() {
        System.out.println("Dispense a gum first.");
    }

    @Override
    public void dispense() {
        System.out.println("The gum has been dispensed.");
        bubbleGumMachine.rollOutGum();
        if (bubbleGumMachine.hasEnoughGums()) {
            bubbleGumMachine.setNextState(this);
        } else {
            bubbleGumMachine.setSoldOutState();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        SoldState that = (SoldState) o;

        if (bubbleGumMachine != null ? !bubbleGumMachine.equals(that.bubbleGumMachine) : that.bubbleGumMachine != null)
            return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = bubbleGumMachine != null ? bubbleGumMachine.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CoinHasBeenPutState{" +
               "bubbleGumMachine=" + bubbleGumMachine +
               ", name='" + name + '\'' +
               '}';
    }
}

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/5/2016
 * Time: 10:39 AM
 */
interface State {

    void putCoin();

    void ejectCoin();

    void turnCrank();

    void dispense();
}
