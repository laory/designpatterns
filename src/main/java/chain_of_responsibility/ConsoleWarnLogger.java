package chain_of_responsibility;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/11/2016
 * Time: 11:09 AM
 */
public class ConsoleWarnLogger extends AbstractLogger {

    public ConsoleWarnLogger() {
        super(Level.WARN);
    }

    @Override
    protected void write(Object logMessage) {
        System.out.println(getClass().getName() + ": " + logMessage);
    }
}
