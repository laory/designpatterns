package chain_of_responsibility;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/11/2016
 * Time: 11:08 AM
 */
public abstract class AbstractLogger implements Logger {

    private static final Level DEFAULT_LEVEL = Level.INFO;

    enum Level {
        DEBUG, INFO, WARN, ERROR
    }

    private final Level          level;
    private       AbstractLogger nextLogger;

    AbstractLogger(Level level) {
        this.level = Optional.ofNullable(level).orElse(DEFAULT_LEVEL);
    }

    public AbstractLogger chain(AbstractLogger nextLogger) {
        Objects.requireNonNull(nextLogger, "Please provide an instance of Logger");
        this.nextLogger = nextLogger;
        return this;
    }

    public AbstractLogger chain(List<AbstractLogger> chainOfLoggers) {
        List<AbstractLogger> loggers = Optional.ofNullable(chainOfLoggers).orElse(Collections.emptyList());
        if (loggers.isEmpty())
            return this;
        int size = loggers.size();
        AbstractLogger head = loggers.get(0);
        List<AbstractLogger> tail = size > 1 ? loggers.subList(1, size) : Collections.emptyList();
        return this.chain(head.chain(tail));
    }

    protected abstract void write(Object logMessage);

    private void log(Level level, Object logMsg) {
        Objects.requireNonNull(level, "Level should be specified");
        if (level.ordinal() <= this.level.ordinal()) {
            try {
                write(logMsg);
            } catch (Exception e) {
                // unable to log message
            }
        } else if (Objects.nonNull(nextLogger)) {
            nextLogger.log(level, logMsg);
        }
    }

    @Override
    public void debug(Object logMsg) {
        log(Level.DEBUG, logMsg);
    }

    @Override
    public void info(Object logMsg) {
        log(Level.INFO, logMsg);
    }

    @Override
    public void warn(Object logMsg) {
        log(Level.WARN, logMsg);
    }

    @Override
    public void error(Object logMsg) {
        log(Level.ERROR, logMsg);
    }

    public static void main(String[] args) {
        AbstractLogger consoleErrLogger = new ConsoleErrLogger();
        AbstractLogger consoleOutLogger = new ConsoleOutLogger();
        AbstractLogger consoleWarnLogger = new ConsoleWarnLogger();
        Logger loggerChain = consoleOutLogger.chain(Arrays.asList(consoleWarnLogger, consoleErrLogger));
        loggerChain.debug("Test debug message");
        loggerChain.info("Test info message");
        loggerChain.warn("Test warning message");
        loggerChain.error(new Exception("Test exception"));
    }
}
