package chain_of_responsibility;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/11/2016
 * Time: 11:09 AM
 */
public class ConsoleErrLogger extends AbstractLogger {

    public ConsoleErrLogger() {
        super(Level.ERROR);
    }

    @Override
    protected void write(Object logMessage) {
        System.err.println(getClass().getName() + ": " + logMessage);
    }
}
