package chain_of_responsibility;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/11/2016
 * Time: 11:31 AM
 */
public interface Logger {

    void debug(Object logMsg);
    void info(Object logMsg);
    void warn(Object logMsg);
    void error(Object logMsg);
}
