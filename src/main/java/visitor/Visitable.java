package visitor;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/11/2016
 * Time: 10:38 AM
 */
public interface Visitable {

    void accept(Visitor visitor);
}
