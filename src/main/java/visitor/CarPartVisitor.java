package visitor;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/11/2016
 * Time: 10:43 AM
 */
public class CarPartVisitor implements Visitor {

    @Override
    public void visit(CarPart visitable) {
        System.out.println("Visitor has visited: " + visitable.getName());
    }

    public static void main(String[] args) {
        CarPart wheel = new Wheel();
        CarPart bumper = new Bumper();
        CarPart steeringWheel = new SteeringWheel();
        CarPartVisitor carPartVisitor = new CarPartVisitor();
        Arrays.asList(wheel, bumper, steeringWheel).forEach(carPart -> carPart.accept(carPartVisitor));
    }
}
