package visitor;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/11/2016
 * Time: 10:40 AM
 */
public abstract class CarPart implements Visitable {

    private final String name;

    public CarPart(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

class Wheel extends CarPart {

    public Wheel() {
        super("A wheel");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}

class Bumper extends CarPart {

    public Bumper() {
        super("A bumper");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}

class SteeringWheel extends CarPart {

    public SteeringWheel() {
        super("A steering wheel");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}