package aggregator.iterator;

import aggregator.menu.MenuComponent;
import aggregator.menu.MenuComposite;

import java.util.Iterator;
import java.util.Objects;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 11:06 AM
 */
public class MenuIterator implements Iterator<MenuComponent> {

    private final Stack<Iterator<MenuComponent>> iterators = new Stack<>();

    public MenuIterator(MenuComponent menuComponent) {
        Objects.requireNonNull(menuComponent);
        Iterator<MenuComponent> iterator = menuComponent.iterator();
        if (Objects.nonNull(iterator))
            iterators.push(iterator);
    }

    @Override
    public boolean hasNext() {
        if (!iterators.isEmpty()) {
            Iterator<MenuComponent> iter = iterators.peek();
            if (iter.hasNext())
                return true;
            else {
                iterators.pop();
                return hasNext();
            }
        }
        return false;
    }

    @Override
    public MenuComponent next() {
        if (hasNext()) {
            Iterator<MenuComponent> iter = iterators.peek();
            MenuComponent next = iter.next();
            if (next instanceof MenuComposite) {
                iterators.push(next.iterator());
            }
            return next;
        }
        return null;
    }
}
