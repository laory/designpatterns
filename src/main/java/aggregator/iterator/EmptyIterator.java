package aggregator.iterator;

import aggregator.menu.MenuComponent;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 11:06 AM
 */
public class EmptyIterator implements Iterator<MenuComponent> {

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public MenuComponent next() {
        return null;
    }
}
