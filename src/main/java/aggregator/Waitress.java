package aggregator;

import aggregator.menu.Menu;
import aggregator.menu.MenuComponent;
import aggregator.menu.MenuComposite;
import aggregator.menu.MenuItem;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:37 AM
 */
public class Waitress {

    public static void main(String[] args) {
        Menu menu1 = new Menu();
        menu1.addItem(new MenuItem("Menu1Item1", 1.3));
        menu1.addItem(new MenuItem("Menu1Item2", 1.65));
        menu1.addItem(new MenuItem("Menu1Item3", 0.5));
        MenuComposite submenu1 = new MenuComposite("Submenu1");
        MenuComposite submenu2 = new MenuComposite("Submenu2");
        menu1.addItem(submenu1);
        menu1.addItem(new MenuItem("Menu1Item5", 987.0));
        menu1.addItem(submenu2);

        submenu1.addComponent(new MenuItem("Submenu1Item1", 123.0));
        submenu1.addComponent(new MenuItem("Submenu1Item2", 3.0));
        submenu1.addComponent(new MenuItem("Submenu1Item3", 123123.0));
        submenu1.addComponent(new MenuItem("Submenu1Item4", 12.0));
        submenu1.addComponent(new MenuItem("Submenu1Item5", 23.0));

        submenu2.addComponent(new MenuItem("Submenu2Item1", 44.0));
        submenu2.addComponent(new MenuItem("Submenu2Item2", 653.0));

        iterate(menu1.iterator());
    }

    private static void iterate(Iterator<MenuComponent> iterator) {
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
