package aggregator.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 11:02 AM
 */
public class MenuComposite implements MenuComponent {

    private final List<MenuComponent> components = new ArrayList<>();
    public final String title;

    public MenuComposite(String title) {
        this.title = title;
    }

    @Override
    public Iterator<MenuComponent> iterator() {
        return components.iterator();
    }

    @Override
    public void addComponent(MenuComponent component) {
        components.add(component);
    }

    @Override
    public String toString() {
        return "MenuComposite{" +
               "title='" + title + '\'' +
               '}';
    }
}
