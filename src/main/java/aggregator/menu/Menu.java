package aggregator.menu;

import aggregator.iterator.MenuIterator;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:05 AM
 */
public class Menu {

    private final MenuComponent root = new MenuComposite("Menu");

    public void addItem(MenuComponent item) {
        root.addComponent(item);
    }

    public MenuIterator iterator() {
        return new MenuIterator(root);
    }
}
