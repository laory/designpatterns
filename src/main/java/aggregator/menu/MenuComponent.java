package aggregator.menu;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 11:03 AM
 */
public interface MenuComponent extends Iterable<MenuComponent> {

    void addComponent(MenuComponent component);
}
