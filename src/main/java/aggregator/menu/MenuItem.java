package aggregator.menu;

import aggregator.iterator.EmptyIterator;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:06 AM
 */
public class MenuItem implements MenuComponent{

    public final String title;
    public final Double price;

    public MenuItem(String title, Double price) {
        this.title = title;
        this.price = price;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
               "title='" + title + '\'' +
               ", price=" + price +
               '}';
    }

    @Override
    public Iterator<MenuComponent> iterator() {
        return new EmptyIterator();
    }

    @Override
    public void addComponent(MenuComponent component) {
        throw new UnsupportedOperationException();
    }
}
