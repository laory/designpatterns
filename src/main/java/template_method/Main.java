package template_method;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/3/2016
 * Time: 10:07 AM
 */
public class Main {

    public static void main(String[] args) {
        AlgorithmOption1 algorithmOption1 = new AlgorithmOption1();
        AlgorithmOption2 algorithmOption2 = new AlgorithmOption2();
        algorithmOption1.performActions();
        algorithmOption2.performActions();
    }
}
