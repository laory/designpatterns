package template_method;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/3/2016
 * Time: 10:05 AM
 */
public class AlgorithmOption1 extends BasicAlgorithm {

    protected void action3() {
        System.out.println("Option 1: action 3");
    }

    protected void action2() {
        System.out.println("Option 1: action 2");
    }
}
