package template_method;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/3/2016
 * Time: 9:58 AM
 */
public abstract class BasicAlgorithm {

    public final void performActions() {
        System.out.println(this.getClass().getName() + " performing actions:");
        action1();
        action2();
        action3();
        hook();
    }

    protected void hook() {
    }

    protected abstract void action3();

    protected abstract void action2();

    private void action1() {
        System.out.println("Basic action 1");
    }
}
