package factory.abstract_factory.store;

import factory.abstract_factory.factory.ChicagoPizzaIngredientFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:13 AM
 */
public class ChicagoPizzaStore extends PizzaStore {

    private final ChicagoPizzaIngredientFactory ingredientFactory = new ChicagoPizzaIngredientFactory();

    @Override
    protected factory.abstract_factory.product.pizza.Pizza createPizza(String type) {
        return pizzaFactory.createPizza(ingredientFactory, type);
    }
}
