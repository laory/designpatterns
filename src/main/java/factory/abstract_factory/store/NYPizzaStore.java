package factory.abstract_factory.store;

import factory.abstract_factory.factory.NYPizzaIngredientFactory;
import factory.abstract_factory.product.pizza.Pizza;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:13 AM
 */
public class NYPizzaStore extends PizzaStore {

    private final NYPizzaIngredientFactory ingredientFactory = new NYPizzaIngredientFactory();

    @Override
    protected Pizza createPizza(String type) {
        return pizzaFactory.createPizza(ingredientFactory, type);
    }
}
