package factory.abstract_factory.store;

import factory.abstract_factory.factory.CaliforniaPizzaIngredientFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:13 AM
 */
public class CaliforniaPizzaStore extends PizzaStore {

    private final CaliforniaPizzaIngredientFactory ingredientFactory = new CaliforniaPizzaIngredientFactory();

    @Override
    protected factory.abstract_factory.product.pizza.Pizza createPizza(String type) {
        return pizzaFactory.createPizza(ingredientFactory, type);
    }
}
