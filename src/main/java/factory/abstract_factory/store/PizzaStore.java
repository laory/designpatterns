package factory.abstract_factory.store;

import factory.abstract_factory.factory.PizzaFactory;
import factory.abstract_factory.product.pizza.Pizza;

import static factory.factory_method.simple_factory.NYPizzaFactory.CHEESE;
import static factory.factory_method.simple_factory.NYPizzaFactory.CLAM;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:35 PM
 */
public abstract class PizzaStore {

    protected final PizzaFactory pizzaFactory = new PizzaFactory();

    public Pizza orderPizza(String type) {
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }

    protected abstract Pizza createPizza(String type);

    public static void main(String[] args) {
        NYPizzaStore nyPizzaStore = new NYPizzaStore();
        ChicagoPizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
        CaliforniaPizzaStore californiaPizzaStore = new CaliforniaPizzaStore();
        nyPizzaStore.orderPizza(CHEESE);
        System.out.println();
        chicagoPizzaStore.orderPizza(CHEESE);
        System.out.println();
        californiaPizzaStore.orderPizza(CHEESE);
        System.out.println();
        System.out.println();
        nyPizzaStore.orderPizza(CLAM);
        System.out.println();
        chicagoPizzaStore.orderPizza(CLAM);
        System.out.println();
        californiaPizzaStore.orderPizza(CLAM);
    }
}
