package factory.abstract_factory.factory;

import factory.abstract_factory.product.ingredient.Cheese;
import factory.abstract_factory.product.ingredient.Clams;
import factory.abstract_factory.product.ingredient.Dough;
import factory.abstract_factory.product.ingredient.Sauce;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:34 AM
 */
public interface PizzaIngredientFactory {

    public Clams createClam();

    public Cheese createCheese();

    public Dough createDough();

    public Sauce createSauce();
}
