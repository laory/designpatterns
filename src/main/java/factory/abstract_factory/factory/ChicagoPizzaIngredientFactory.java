package factory.abstract_factory.factory;

import factory.abstract_factory.product.ingredient.Cheese;
import factory.abstract_factory.product.ingredient.Clams;
import factory.abstract_factory.product.ingredient.Dough;
import factory.abstract_factory.product.ingredient.Sauce;
import factory.abstract_factory.product.ingredient.chicago.FrozenClams;
import factory.abstract_factory.product.ingredient.chicago.MozzarellaCheese;
import factory.abstract_factory.product.ingredient.chicago.PlumTomatoSauce;
import factory.abstract_factory.product.ingredient.chicago.ThickCrustDough;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:52 AM
 */
public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {

    @Override
    public Clams createClam() {
        return new FrozenClams();
    }

    @Override
    public Cheese createCheese() {
        return new MozzarellaCheese();
    }

    @Override
    public Dough createDough() {
        return new ThickCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new PlumTomatoSauce();
    }
}
