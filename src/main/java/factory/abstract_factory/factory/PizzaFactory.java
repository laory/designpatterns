package factory.abstract_factory.factory;

import factory.abstract_factory.product.pizza.CheesePizza;
import factory.abstract_factory.product.pizza.ClamPizza;
import factory.abstract_factory.product.pizza.Pizza;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:18 PM
 */
public class PizzaFactory {

    public static final String CHEESE    = "cheese";
    public static final String PEPPERONI = "pepperoni";
    public static final String CLAM      = "clam";
    public static final String VEGGIE    = "veggie";

    public Pizza createPizza(PizzaIngredientFactory ingredientFactory, String type) {
        switch (type) {
            case CHEESE:
                return new CheesePizza(ingredientFactory);
            case CLAM:
                return new ClamPizza(ingredientFactory);
            default:
                throw new IllegalArgumentException("Unknown pizza type: " + type);
        }
    }
}
