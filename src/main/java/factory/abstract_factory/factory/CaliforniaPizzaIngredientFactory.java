package factory.abstract_factory.factory;

import factory.abstract_factory.product.ingredient.Cheese;
import factory.abstract_factory.product.ingredient.Clams;
import factory.abstract_factory.product.ingredient.Dough;
import factory.abstract_factory.product.ingredient.Sauce;
import factory.abstract_factory.product.ingredient.california.BruschettaSauce;
import factory.abstract_factory.product.ingredient.california.Calamari;
import factory.abstract_factory.product.ingredient.california.GoatCheese;
import factory.abstract_factory.product.ingredient.california.VeryThinCrust;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:52 AM
 */
public class CaliforniaPizzaIngredientFactory implements PizzaIngredientFactory {

    @Override
    public Clams createClam() {
        return new Calamari();
    }

    @Override
    public Cheese createCheese() {
        return new GoatCheese();
    }

    @Override
    public Dough createDough() {
        return new VeryThinCrust();
    }

    @Override
    public Sauce createSauce() {
        return new BruschettaSauce();
    }
}
