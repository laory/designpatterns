package factory.abstract_factory.factory;

import factory.abstract_factory.product.ingredient.Cheese;
import factory.abstract_factory.product.ingredient.Clams;
import factory.abstract_factory.product.ingredient.Dough;
import factory.abstract_factory.product.ingredient.Sauce;
import factory.abstract_factory.product.ingredient.new_york.FreshClams;
import factory.abstract_factory.product.ingredient.new_york.MarinaraSauce;
import factory.abstract_factory.product.ingredient.new_york.ReggianoCheese;
import factory.abstract_factory.product.ingredient.new_york.ThinCrustDough;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:52 AM
 */
public class NYPizzaIngredientFactory implements PizzaIngredientFactory {

    @Override
    public Clams createClam() {
        return new FreshClams();
    }

    @Override
    public Cheese createCheese() {
        return new ReggianoCheese();
    }

    @Override
    public Dough createDough() {
        return new ThinCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new MarinaraSauce();
    }
}
