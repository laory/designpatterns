package factory.abstract_factory.product.ingredient.california;

import factory.abstract_factory.product.ingredient.Dough;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:32 AM
 */
public class VeryThinCrust extends Dough {}
