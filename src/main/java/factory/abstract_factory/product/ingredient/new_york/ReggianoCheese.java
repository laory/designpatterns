package factory.abstract_factory.product.ingredient.new_york;

import factory.abstract_factory.product.ingredient.Cheese;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:31 AM
 */
public class ReggianoCheese extends Cheese {}
