package factory.abstract_factory.product.pizza;

import factory.abstract_factory.product.ingredient.Cheese;
import factory.abstract_factory.product.ingredient.Clams;
import factory.abstract_factory.product.ingredient.Dough;
import factory.abstract_factory.product.ingredient.Sauce;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:19 PM
 */
public abstract class Pizza {

    public final String name;
    public final Dough  dough;
    public final Sauce  sauce;
    public final Clams  clam;
    public final Cheese cheese;

    public Pizza(String name, Dough dough, Sauce sauce, Clams clam, Cheese cheese) {
        this.name = name;
        this.dough = dough;
        this.sauce = sauce;
        this.clam = clam;
        this.cheese = cheese;
    }

    public void prepare() {
        System.out.println("Preparing " + name);
        System.out.println("Tossing dough: " + dough);
        System.out.println("Adding sauce: " + sauce);
        System.out.println("Adding clam: " + clam);
        System.out.println("Adding cheese: " + cheese);
    }

    public void bake() {
        System.out.println("Bake for 25 minutes at 350");
    }

    public void cut() {
        System.out.println("Cutting the pizza into diagonal slices");
    }

    public void box() {
        System.out.println("Place pizza in official PizzaStore box");
    }
}
