package factory.abstract_factory.product.pizza;

import factory.abstract_factory.factory.PizzaIngredientFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:45 AM
 */
public class CheesePizza extends Pizza {

    public CheesePizza(PizzaIngredientFactory ingredientFactory) {
        super("Cheese pizza", ingredientFactory.createDough(), ingredientFactory.createSauce(),
              null, ingredientFactory.createCheese());
    }
}
