package factory.factory_method;

import factory.factory_method.simple_factory.ChicagoPizzaFactory;
import factory.factory_method.product.Pizza;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:13 AM
 */
public class ChicagoPizzaStore extends PizzaStore {

    private final ChicagoPizzaFactory factory = new ChicagoPizzaFactory();

    @Override
    protected Pizza createPizza(String type) {
        return factory.createPizza(type);
    }
}
