package factory.factory_method;

import factory.factory_method.product.Pizza;

import static factory.factory_method.simple_factory.NYPizzaFactory.CHEESE;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:35 PM
 */
public abstract class PizzaStore {

    public Pizza orderPizza(String type) {
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }

    protected abstract Pizza createPizza(String type);

    public static void main(String[] args) {
        NYPizzaStore nyPizzaStore = new NYPizzaStore();
        ChicagoPizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
        Pizza nyCheesePizza = nyPizzaStore.orderPizza(CHEESE);
        System.out.println();
        Pizza chicagoCheesePizza = chicagoPizzaStore.orderPizza(CHEESE);
    }
}
