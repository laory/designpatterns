package factory.factory_method.simple_factory;

import factory.factory_method.product.*;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:18 PM
 */
public class ChicagoPizzaFactory {

    public static final String CHEESE    = "cheese";
    public static final String PEPPERONI = "pepperoni";
    public static final String CLAM      = "clam";
    public static final String VEGGIE    = "veggie";

    public Pizza createPizza(String type) {
        switch (type) {
            case CHEESE:
                return new ChicagoStyleCheesePizza();
            case PEPPERONI:
                return new ChicagoStylePepperoniPizza();
            case CLAM:
                return new ChicagoStyleClamPizza();
            case VEGGIE:
                return new ChicagoStyleVeggiePizza();
            default:
                throw new IllegalArgumentException("Unknown pizza type: " + type);
        }
    }
}
