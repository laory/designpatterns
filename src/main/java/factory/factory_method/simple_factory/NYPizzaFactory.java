package factory.factory_method.simple_factory;

import factory.factory_method.product.*;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:18 PM
 */
public class NYPizzaFactory {

    public static final String CHEESE    = "cheese";
    public static final String PEPPERONI = "pepperoni";
    public static final String CLAM      = "clam";
    public static final String VEGGIE    = "veggie";

    public Pizza createPizza(String type) {
        switch (type) {
            case CHEESE:
                return new NYStyleCheesePizza();
            case PEPPERONI:
                return new NYStylePepperoniPizza();
            case CLAM:
                return new NYStyleClamPizza();
            case VEGGIE:
                return new NYStyleVeggiePizza();
            default:
                throw new IllegalArgumentException("Unknown pizza type: " + type);
        }
    }
}
