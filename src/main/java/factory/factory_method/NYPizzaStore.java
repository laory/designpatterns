package factory.factory_method;

import factory.factory_method.simple_factory.NYPizzaFactory;
import factory.factory_method.product.Pizza;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/9/16
 * Time: 12:13 AM
 */
public class NYPizzaStore extends PizzaStore {

    private final NYPizzaFactory factory = new NYPizzaFactory();

    @Override
    protected Pizza createPizza(String type) {
        return factory.createPizza(type);
    }
}
