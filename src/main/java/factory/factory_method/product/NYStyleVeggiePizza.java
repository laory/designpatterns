package factory.factory_method.product;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:23 PM
 */
public class NYStyleVeggiePizza extends Pizza {

    public NYStyleVeggiePizza() {
        super("NY Style Veggie Pizza", "Dough", "Sauce",
              Collections.singletonList("Veggies"));
    }
}
