package factory.factory_method.product;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:23 PM
 */
public class NYStyleCheesePizza extends Pizza {

    public NYStyleCheesePizza() {
        super("NY Style Sauce and Cheese Pizza", "Thin Crust Dough", "Marinara Sauce",
              Collections.singletonList("Grated Reggiano Cheese"));
    }
}
