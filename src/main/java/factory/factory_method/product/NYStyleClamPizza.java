package factory.factory_method.product;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:23 PM
 */
public class NYStyleClamPizza extends Pizza {

    public NYStyleClamPizza() {
        super("NY Style Clam Pizza", "Dough", "Sauce",
              Collections.singletonList("Clam"));
    }
}
