package factory.factory_method.product;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:23 PM
 */
public class NYStylePepperoniPizza extends Pizza {

    public NYStylePepperoniPizza() {
        super("NY Style Pepperoni Pizza", "Dough", "Sauce",
              Collections.singletonList("Pepperoni"));
    }
}
