package factory.simple_factory;

import factory.simple_factory.product.*;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:18 PM
 */
public class PizzaFactory {

    public static final String CHEESE = "cheese";
    public static final String PEPPERONI = "pepperoni";
    public static final String CLAM = "clam";
    public static final String VEGGIE = "veggie";

    public Pizza createPizza(String type) {
        switch (type) {
            case CHEESE:
                return new CheesePizza();
            case PEPPERONI:
                return new PepperoniPizza();
            case CLAM:
                return new ClamPizza();
            case VEGGIE:
                return new VeggiePizza();
            default:
                throw new IllegalArgumentException("Unknown pizza type: " + type);
        }
    }
}
