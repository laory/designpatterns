package factory.simple_factory.product;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:23 PM
 */
public class VeggiePizza extends Pizza {

    public VeggiePizza() {
        super("Veggie Pizza", "Dough", "Sauce",
              Collections.singletonList("Veggies"));
    }
}
