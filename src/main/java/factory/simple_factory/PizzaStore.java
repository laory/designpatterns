package factory.simple_factory;

import factory.simple_factory.product.Pizza;

import static factory.simple_factory.PizzaFactory.CHEESE;
import static factory.simple_factory.PizzaFactory.PEPPERONI;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 23/9/16
 * Time: 11:35 PM
 */
public class PizzaStore {

    private final PizzaFactory pizzaFactory;

    public PizzaStore(PizzaFactory pizzaFactory) {
        this.pizzaFactory = pizzaFactory;
    }

    public Pizza orderPizza(String type) {
        Pizza pizza = pizzaFactory.createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }

    public static void main(String[] args) {
        PizzaStore pizzaStore = new PizzaStore(new PizzaFactory());
        pizzaStore.orderPizza(PEPPERONI);
        System.out.println();
        pizzaStore.orderPizza(CHEESE);
    }
}
