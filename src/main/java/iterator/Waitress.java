package iterator;

import iterator.menu.Menu1;
import iterator.menu.Menu2;
import iterator.menu.MenuItem;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:37 AM
 */
public class Waitress {

    public static void main(String[] args) {
        Menu1 menu1 = new Menu1();
        Menu2 menu2 = new Menu2();
        menu1.addItem(new MenuItem("Menu1Item1", 1.3));
        menu1.addItem(new MenuItem("Menu1Item2", 1.65));
        menu1.addItem(new MenuItem("Menu1Item3", 0.5));

        menu2.addItem(new MenuItem("Menu2Item1", 1.0));
        menu2.addItem(new MenuItem("Menu2Item2", 2.0));
        menu2.addItem(new MenuItem("Menu2Item3", 4.0));
        menu2.addItem(new MenuItem("Menu2Item4", 6.0));

        iterate(menu1.menuIterator());
        System.out.println();
        iterate(menu2.menuIterator());
        System.out.println();
        iterate(menu1.iterator());
        System.out.println();
        iterate(menu2.iterator());
        System.out.println();
    }

    private static void iterate(Iterator<MenuItem> iterator) {
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    private static void iterate(java.util.Iterator<MenuItem> iterator) {
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
