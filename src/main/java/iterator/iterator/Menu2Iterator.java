package iterator.iterator;

import iterator.Iterator;
import iterator.menu.MenuItem;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:33 AM
 */
public class Menu2Iterator implements Iterator<MenuItem> {

    private final MenuItem[] menu;
    private int i = 0;

    public Menu2Iterator(MenuItem[] menu) {
        Objects.requireNonNull(menu, "Shouldn't be null");
        this.menu = menu;
    }

    @Override
    public boolean hasNext() {
        return menu.length > i;
    }

    @Override
    public MenuItem next() {
        return hasNext() ? menu[i++] : null;
    }
}
