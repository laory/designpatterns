package iterator.iterator;

import iterator.Iterator;
import iterator.menu.MenuItem;

import java.util.List;
import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:15 AM
 */
public class Menu1Iterator implements Iterator<MenuItem> {

    private final List<MenuItem> items;
    private int i = 0;

    public Menu1Iterator(List<MenuItem> items) {
        Objects.requireNonNull(items, "Shouldn't be null");
        this.items = items;
    }

    @Override
    public boolean hasNext() {
        return items.size() > i;
    }

    @Override
    public MenuItem next() {
        return hasNext() ? items.get(i++) : null;
    }
}
