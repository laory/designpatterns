package iterator.iterator;

import iterator.menu.MenuItem;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:54 AM
 */
public class Menu2IteratorJdk implements java.util.Iterator<MenuItem> {

    private final MenuItem[] menu;
    private int i = 0;

    public Menu2IteratorJdk(MenuItem[] menu) {
        Objects.requireNonNull(menu, "Shouldn't be null");
        this.menu = menu;
    }

    @Override
    public boolean hasNext() {
        return menu.length > i;
    }

    @Override
    public MenuItem next() {
        return hasNext() ? menu[i++] : null;
    }
}
