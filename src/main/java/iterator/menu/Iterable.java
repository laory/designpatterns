package iterator.menu;

import iterator.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:52 AM
 */
public interface Iterable <T> {

    Iterator<T> menuIterator();
}
