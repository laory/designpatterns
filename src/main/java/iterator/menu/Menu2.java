package iterator.menu;

import iterator.Iterator;
import iterator.iterator.Menu1Iterator;
import iterator.iterator.Menu2Iterator;
import iterator.iterator.Menu2IteratorJdk;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:05 AM
 */
public class Menu2 implements Iterable, java.lang.Iterable<MenuItem> {

    private final MenuItem[] menu = new MenuItem[10];
    private       int        size = 0;

    public void addItem(MenuItem item) {
        if (menu.length > size) {
            menu[size++] = item;
        }
    }

    public Iterator<MenuItem> menuIterator() {
        return new Menu2Iterator(menu);
    }

    @Override
    public java.util.Iterator<MenuItem> iterator() {
        return new Menu2IteratorJdk(menu);
    }
}
