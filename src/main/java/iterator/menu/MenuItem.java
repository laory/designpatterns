package iterator.menu;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:06 AM
 */
public class MenuItem {

    public final String title;
    public final Double price;

    public MenuItem(String title, Double price) {
        this.title = title;
        this.price = price;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
               "title='" + title + '\'' +
               ", price=" + price +
               '}';
    }
}
