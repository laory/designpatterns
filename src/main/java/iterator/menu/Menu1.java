package iterator.menu;

import iterator.Iterator;
import iterator.iterator.Menu1Iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:05 AM
 */
public class Menu1 implements Iterable, java.lang.Iterable<MenuItem> {

    private final List<MenuItem> menu = new ArrayList<>();

    public void addItem(MenuItem item) {
        menu.add(item);
    }

    @Override
    public Iterator<MenuItem> menuIterator() {
        return new Menu1Iterator(menu);
    }

    @Override
    public java.util.Iterator<MenuItem> iterator() {
        return menu.iterator();
    }
}
