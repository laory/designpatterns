package iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/4/2016
 * Time: 10:02 AM
 */
public interface Iterator<T> {

    boolean hasNext();

    T next();
}
