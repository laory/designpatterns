package facade.devices;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:48 PM
 */
public class SecurityControl {

    private static final String name = "Security control";

    public void arm() {
        System.out.println(name + " is armed");
    }

    public void disarm() {
        System.out.println(name + " is disarmed");
    }
}
