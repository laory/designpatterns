package facade;

import facade.devices.*;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 01/10/16
 * Time: 8:15 PM
 */
public class Facade {

    private final CeilingLight ceilingLight;
    private final GardenLight gardenLight;
    private final OutdoorLight outdoorLight;
    private final SecurityControl securityControl;
    private final Stereo stereo;

    public Facade(CeilingLight ceilingLight, GardenLight gardenLight, OutdoorLight outdoorLight, SecurityControl securityControl, Stereo stereo) {
        this.ceilingLight = ceilingLight;
        this.gardenLight = gardenLight;
        this.outdoorLight = outdoorLight;
        this.securityControl = securityControl;
        this.stereo = stereo;
    }

    public void eveningParty() {
        System.out.println("Starting evening party:");
        ceilingLight.off();
        gardenLight.setDawnTime();
        gardenLight.manualOn();
        outdoorLight.on();
        securityControl.disarm();
        stereo.on();
        stereo.setCd(1);
        stereo.setVolume(100);
    }

    public void turnEverythingOff() {
        System.out.println("Turning everything off:");
        ceilingLight.off();
        gardenLight.manualOff();
        outdoorLight.off();
        stereo.off();
        securityControl.arm();
    }


    public static void main(String[] args) {
        Facade facade = new Facade(new CeilingLight(), new GardenLight(), new OutdoorLight(), new SecurityControl(),
                                   new Stereo());
        facade.eveningParty();
        facade.turnEverythingOff();
    }
}
