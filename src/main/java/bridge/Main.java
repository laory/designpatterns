package bridge;

import bridge.manufacturer.AVK;
import bridge.manufacturer.Roshen;
import bridge.manufacturer.Svitoch;
import bridge.manufacturer.SweetsManufacturer;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 10:48 AM
 */
public class Main {

    public static void main(String[] args) {
        SweetsManufacturer avk = new AVK();
        SweetsManufacturer roshen = new Roshen();
        SweetsManufacturer svitoch = new Svitoch();
        SweetProvider avkCakeProvider = new CakeProvider(avk);
        SweetProvider roshenCakeProvider = new CakeProvider(roshen);
        SweetProvider svitochCakeProvider = new CakeProvider(svitoch);
        SweetProvider avkCandyProvider = new CandyProvider(avk);
        SweetProvider roshenCandyProvider = new CandyProvider(roshen);
        SweetProvider svitochCandyProvider = new CandyProvider(svitoch);

        avkCakeProvider.provideSweet();
        avkCakeProvider.packSweets();
        roshenCakeProvider.provideSweet();
        roshenCakeProvider.packSweets();
        svitochCakeProvider.provideSweet();
        svitochCakeProvider.packSweets();
        avkCandyProvider.provideSweet();
        avkCandyProvider.packSweets();
        roshenCandyProvider.provideSweet();
        roshenCandyProvider.packSweets();
        svitochCandyProvider.provideSweet();
        svitochCandyProvider.packSweets();
    }
}
