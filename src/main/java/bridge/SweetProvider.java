package bridge;

import bridge.manufacturer.SweetsManufacturer;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 10:25 AM
 */
public abstract class SweetProvider {

    protected final SweetsManufacturer sweetsManufacturer;

    public SweetProvider(SweetsManufacturer sweetsManufacturer) {
        this.sweetsManufacturer = sweetsManufacturer;
    }

    public abstract void provideSweet();

    public void packSweets() {
        System.out.println("Packing sweets");
        System.out.println();
    }
}
