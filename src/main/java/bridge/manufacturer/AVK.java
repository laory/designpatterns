package bridge.manufacturer;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 10:28 AM
 */
public class AVK implements SweetsManufacturer {

    @Override
    public void createCandy() {
        System.out.println("Using secret AVK recipe");
        System.out.println("AVK candy has been created");
    }

    @Override
    public void createCake() {
        System.out.println("Using secret AVK recipe");
        System.out.println("AVK cake has been created");
    }
}
