package bridge.manufacturer;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 10:28 AM
 */
public class Roshen implements SweetsManufacturer {

    @Override
    public void createCandy() {
        System.out.println("Using secret Roshen recipe");
        System.out.println("Roshen candy has been created");
    }

    @Override
    public void createCake() {
        System.out.println("Using secret Roshen recipe");
        System.out.println("Roshen cake has been created");
    }
}
