package bridge.manufacturer;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 10:27 AM
 */
public interface SweetsManufacturer {

    void createCandy();
    void createCake();
}
