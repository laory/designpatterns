package bridge.manufacturer;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 10:28 AM
 */
public class Svitoch implements SweetsManufacturer {

    @Override
    public void createCandy() {
        System.out.println("Using secret Svitoch recipe");
        System.out.println("Svitoch candy has been created");
    }

    @Override
    public void createCake() {
        System.out.println("Using secret Svitoch recipe");
        System.out.println("Svitoch cake has been created");
    }
}
