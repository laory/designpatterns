package bridge;

import bridge.manufacturer.SweetsManufacturer;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 10:32 AM
 */
public class CakeProvider extends SweetProvider {

    public CakeProvider(SweetsManufacturer sweetsManufacturer) {
        super(sweetsManufacturer);
    }

    @Override
    public void provideSweet() {
        sweetsManufacturer.createCake();
    }
}
