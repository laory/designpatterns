package observer;

import java.util.Map;

/**
 * Created by Hedgehog on 23/1/16.
 */
public interface DisplayElement<T> {

    void display(Map<String, T> data);
}
