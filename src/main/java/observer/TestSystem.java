package observer;

import observer.observable.WeatherData;
import observer.observer.CurrConditionsDisplay;
import observer.observer.StatisticsDisplay;

/**
 * Created by Hedgehog on 30/1/16.
 */
public class TestSystem {

    public static void main(String[] args) throws InterruptedException {
        final WeatherData weatherData = new WeatherData();

        final CurrConditionsDisplay currConditionsDisplay = new CurrConditionsDisplay();
        final StatisticsDisplay statisticsDisplay = new StatisticsDisplay();

        weatherData.addObserver(currConditionsDisplay);

        final Thread updatingThread = new Thread(() -> {
            while (true) {
                weatherData.measurementsChanged();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        updatingThread.start();

        Thread.sleep(5000);
        weatherData.addObserver(statisticsDisplay);

        Thread.sleep(5000);
        weatherData.deleteObserver(currConditionsDisplay);
        Thread.sleep(5000);
        weatherData.deleteObserver(statisticsDisplay);
    }
}
