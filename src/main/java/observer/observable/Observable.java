package observer.observable;

import observer.observer.Observer;

/**
 * Created by Hedgehog on 23/1/16.
 */
public interface Observable {

    void addObserver(Observer observer);

    void deleteObserver(Observer observer);

    void notifyObservers();

    void setChanged();
}
