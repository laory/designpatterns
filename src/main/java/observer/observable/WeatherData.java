package observer.observable;

import observer.observer.Observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class WeatherData implements Observable {

    public static final int    MAX_TEMP     = 32;
    public static final int    MAX_HUMIDITY = 20;
    public static final int    MAX_PRESSURE = 120;
    public static final String TEMPERATURE  = "Temperature";
    public static final String HUMIDITY     = "Humidity";
    public static final String PRESSURE     = "Pressure";

    private final    List<Observer> observers = new ArrayList<>();
    private volatile boolean        changed   = false;

    @Override
    public void addObserver(Observer observer) {
        if (!observers.contains(observer))
            observers.add(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        if (changed) {
            final HashMap<String, Object> data = new HashMap<String, Object>() {{
                put(TEMPERATURE, getTemperature());
                put(HUMIDITY, getHumidity());
                put(PRESSURE, getPressure());
            }};
            observers.forEach(observer -> observer.update(this, data));
        }
    }

    @Override
    public void setChanged() {
        changed = true;
    }

    public double getTemperature() {
        return randomVal(MAX_TEMP);
    }

    public double getHumidity() {
        return randomVal(MAX_HUMIDITY);
    }

    public double getPressure() {
        return randomVal(MAX_PRESSURE);
    }

    public void measurementsChanged() {
        if (ThreadLocalRandom.current().nextBoolean()) {
            setChanged();
        }
        notifyObservers();
    }

    private double randomVal(double maxVal) {return ThreadLocalRandom.current().nextDouble(maxVal);}
}
