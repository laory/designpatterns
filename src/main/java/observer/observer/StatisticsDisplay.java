package observer.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Hedgehog on 30/1/16.
 */
public class StatisticsDisplay extends ConditionsDisplay<Double> {

    public static final String                    NAME             = "Statistics display";
    public static final int                       INITIAL_CAPACITY = 10;
    private final       List<Map<String, Double>> dataArchive      = new ArrayList<>(INITIAL_CAPACITY);
    private             int                       iterator         = 0;

    public StatisticsDisplay() {
        super(NAME);
        IntStream.range(0, INITIAL_CAPACITY).forEach(i -> dataArchive.add(null));
    }

    @Override
    public Map<String, Double> processData(Map<String, Double> data) {
        final Map<String, Double> processedData = super.processData(data);
        addToArchive(processedData);
        return calculateStatistics();
    }

    private Map<String, Double> calculateStatistics() {
        final long size = dataArchive.stream().filter(Objects:: nonNull).count();
        final Map<String, Double> sumOfValues = dataArchive.stream().filter(Objects:: nonNull).flatMap(
                statisticsData -> statisticsData.entrySet().stream()).collect(
                Collectors.toMap(Map.Entry:: getKey, Map.Entry:: getValue, (a, b) -> a + b));
        return sumOfValues.entrySet().stream().collect(
                Collectors.toMap(Map.Entry:: getKey, entry -> entry.getValue() / size));
    }

    private void addToArchive(Map<String, Double> data) {
        dataArchive.set(iterator, data);
        iterator = (iterator + 1) % INITIAL_CAPACITY;
    }
}
