package observer.observer;

import observer.observable.Observable;

import java.util.Map;

/**
 * Created by Hedgehog on 23/1/16.
 */
public interface Observer<T> {

    void update(Observable observable, Map<String, T> data);
}
