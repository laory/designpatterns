package observer.observer;

import observer.DisplayElement;
import observer.observable.Observable;
import observer.observable.WeatherData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hedgehog on 23/1/16.
 */
public abstract class ConditionsDisplay<T> implements Observer<T>, DisplayElement<T> {

    private final String name;

    public ConditionsDisplay(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable observable, Map<String, T> data) {
        if (observable instanceof WeatherData) {
            Map<String, T> processedData = processData(data);
            display(processedData);
        }
    }

    @Override
    public void display(Map<String, T> data) {
        System.out.println(name + ": " + data);
    }

    public Map<String, T> processData(Map<String, T> data) {
        final HashMap<String, T> dataCopy = new HashMap<>();
        dataCopy.putAll(data);
        return dataCopy;
    }
}
