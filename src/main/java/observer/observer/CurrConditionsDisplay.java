package observer.observer;

/**
 * Created by Hedgehog on 30/1/16.
 */
public class CurrConditionsDisplay extends ConditionsDisplay<Double> {

    public static final String NAME = "Current conditions display";

    public CurrConditionsDisplay() {
        super(NAME);
    }

}
