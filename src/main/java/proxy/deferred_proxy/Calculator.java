package proxy.deferred_proxy;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/6/2016
 * Time: 10:15 AM
 */
public class Calculator {

    public void performToughCalculations() {
        System.out.println("Calculated =)");
    }
}
