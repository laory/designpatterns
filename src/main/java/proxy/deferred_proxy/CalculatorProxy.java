package proxy.deferred_proxy;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/6/2016
 * Time: 10:15 AM
 */
public class CalculatorProxy extends Calculator {

    private volatile Calculator calculator;
    private volatile boolean isInProcess = false;

    public void performToughCalculations() {
        if (Objects.isNull(calculator)) {
            System.out.println("Calculating...");
            if (!isInProcess) {
                synchronized(CalculatorProxy.class) {
                    if (!isInProcess) {
                        isInProcess = true;
                        new Thread(() -> {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            calculator = new Calculator();
                        }).start();
                    }
                }
            }
        } else {
            calculator.performToughCalculations();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Calculator calculator = new CalculatorProxy();
        calculator.performToughCalculations();
        calculator.performToughCalculations();
        Thread.sleep(1000);
        calculator.performToughCalculations();
        calculator.performToughCalculations();
        Thread.sleep(4000);
        calculator.performToughCalculations();
        calculator.performToughCalculations();
        Thread.sleep(1000);
        calculator.performToughCalculations();
        calculator.performToughCalculations();
    }
}
