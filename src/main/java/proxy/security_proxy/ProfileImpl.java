package proxy.security_proxy;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/6/2016
 * Time: 10:52 AM
 */
public class ProfileImpl implements Profile {

    private String name;
    private int likes = 0;

    public ProfileImpl(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void like() {
        likes++;
    }

    @Override
    public int getLikes() {
        return likes;
    }
}
