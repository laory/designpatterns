package proxy.security_proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/6/2016
 * Time: 10:55 AM
 */
public class ProfileFactory {

    public static Profile getOwnerProfile(Profile profile) {
        return getProxy(profile, (proxy, method, args) -> {
            System.out.println("Owner method: " + method.getName());
            if (method.getName().equals("like")) {
                throw new IllegalAccessException("You can not like your profile");
            } else {
                return method.invoke(profile, args);
            }
        });
    }

    public static Profile getFriendProfile(Profile profile) {
        return getProxy(profile, (proxy, method, args) -> {
            System.out.println("Friend method: " + method.getName());
            if (method.getName().startsWith("set")) {
                throw new IllegalAccessException("You can not change a profile of your friend");
            } else {
                return method.invoke(profile, args);
            }
        });
    }

    private static Profile getProxy(Profile profile, InvocationHandler invocationHandler) {
        return (Profile) Proxy.newProxyInstance(profile.getClass().getClassLoader(), profile.getClass().getInterfaces(),
                                                    invocationHandler);
    }
}
