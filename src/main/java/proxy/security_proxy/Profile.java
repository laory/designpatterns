package proxy.security_proxy;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/6/2016
 * Time: 11:45 AM
 */
public interface Profile {

    void setName(String name);
    String getName();
    void like();
    int getLikes();
}
