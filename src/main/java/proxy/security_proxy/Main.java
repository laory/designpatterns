package proxy.security_proxy;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/6/2016
 * Time: 11:08 AM
 */
public class Main {

    public static void main(String[] args) {
        Profile profile1 = new ProfileImpl("Name 1");
        Profile profile2 = new ProfileImpl("Name 2");

        Profile ownerProfile1 = ProfileFactory.getOwnerProfile(profile1);
        Profile friendProfile2 = ProfileFactory.getFriendProfile(profile2);

        try {
            ownerProfile1.setName(profile1.getName() + " new");
            ownerProfile1.setName(profile1.getName() + " new");
            System.out.println(ownerProfile1.getName());
            ownerProfile1.like();
        } catch (Exception e) {
            System.out.println(e.getCause());
        }
        try {
            friendProfile2.like();
            friendProfile2.like();
            System.out.println(friendProfile2.getLikes());
            friendProfile2.setName(profile1.getName() + " new");
        } catch (Exception e) {
            System.out.println(e.getCause());
        }
    }
}
