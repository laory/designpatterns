package strategy.character;

import strategy.behavior.AxeBehavior;
import strategy.behavior.KnifeBehavior;
import strategy.behavior.SwordBehavior;

import java.util.Arrays;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class Troll extends Character {

    public Troll() {
        super("Troll", Arrays.asList(new KnifeBehavior(), new SwordBehavior(), new AxeBehavior()));
    }
}
