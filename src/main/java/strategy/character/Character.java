package strategy.character;

import strategy.behavior.WeaponBehavior;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hedgehog on 23/1/16.
 */
public abstract class Character {

    public final String name;
    private final List<WeaponBehavior> weapons = new ArrayList<>();

    public Character(String name, List<WeaponBehavior> weapons) {
        this.name = name;
        this.weapons.addAll(weapons);
    }

    public void fight() {
        weapons.forEach(weapon -> {
            System.out.print(String.format("%s can ", name));
            weapon.useWeapon();
        });
    }
}
