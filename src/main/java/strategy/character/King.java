package strategy.character;

import strategy.behavior.KnifeBehavior;
import strategy.behavior.SwordBehavior;

import java.util.Arrays;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class King extends Character {

    public King() {
        super("King", Arrays.asList(new KnifeBehavior(), new SwordBehavior()));
    }
}
