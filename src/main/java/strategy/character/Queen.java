package strategy.character;

import strategy.behavior.KnifeBehavior;

import java.util.Collections;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class Queen extends Character {

    public Queen() {
        super("Queen", Collections.singletonList(new KnifeBehavior()));
    }
}
