package strategy.behavior;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class SwordBehavior implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Damage with sword!");
    }
}
