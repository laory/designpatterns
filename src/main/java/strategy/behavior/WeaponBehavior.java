package strategy.behavior;

/**
 * Created by Hedgehog on 23/1/16.
 */
public interface WeaponBehavior {

    void useWeapon();
}
