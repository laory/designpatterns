package strategy.behavior;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class BowAndArrowBehavior implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Damage with arrow from bow!");
    }
}
