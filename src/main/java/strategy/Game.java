package strategy;

import strategy.character.*;
import strategy.character.Character;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class Game {

    public static void main(String[] args) {
        Character king = new King();
        Character queen = new Queen();
        Character troll = new Troll();
        Character knight = new Knight();
        king.fight();
        queen.fight();
        troll.fight();
        knight.fight();
    }
}
