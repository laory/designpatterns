package singleton;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 26/9/16
 * Time: 12:15 AM
 */
public class CheckEnumSingleton {

    public static void main(String[] args) {
        System.out.println("Test1");
        System.out.println("Test2");
        System.out.println(EnumSingleton.INSTANCE_2);
        System.out.println("Test3");
        System.out.println(EnumSingleton.INSTANCE_1);
    }
}
