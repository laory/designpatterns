package singleton;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 26/9/16
 * Time: 12:07 AM
 */
public enum EnumSingleton {

    INSTANCE_1("instance_1"),
    INSTANCE_2("instance_2");

    EnumSingleton(String label) {
        System.out.println("Hello: " + label);
    }
}