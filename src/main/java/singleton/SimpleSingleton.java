package singleton;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 25/9/16
 * Time: 11:59 PM
 */
public class SimpleSingleton {

    private static SimpleSingleton instance;

    private SimpleSingleton() {}

    public static SimpleSingleton getInstance() {
        if (Objects.isNull(instance)) {
            instance = new SimpleSingleton();
        }
        return instance;
    }
}
