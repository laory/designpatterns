package singleton;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 25/9/16
 * Time: 11:59 PM
 */
public class StaticSingleton {

    public static final StaticSingleton instance = new StaticSingleton();

    private StaticSingleton() {}
}
