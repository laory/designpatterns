package singleton;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 25/9/16
 * Time: 11:59 PM
 */
public class InitializeOnDemandSingleton {

    static {
        System.out.println("Singleton2: static init");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private InitializeOnDemandSingleton() {
        System.out.println("Singleton2: instance has been created");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void dummyMethod() {
        System.out.println("Singleton2: static method doing some fake job");
    }

    public void instanceDummyMethod() {
        System.out.println("Singleton2.instance: doing some fake job");
    }

    public static InitializeOnDemandSingleton getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {

        static {
            System.out.println("SingletonHolder: pre init");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private static final InitializeOnDemandSingleton INSTANCE = new InitializeOnDemandSingleton();

        static {
            System.out.println("SingletonHolder: post init");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

class TestSingleton2 {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Program started");
        Thread.sleep(1000);
        InitializeOnDemandSingleton.dummyMethod();
        Thread.sleep(1000);
        InitializeOnDemandSingleton instance = InitializeOnDemandSingleton.getInstance();
        Thread.sleep(1000);
        instance.instanceDummyMethod();
    }
}
