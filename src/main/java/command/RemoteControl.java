package command;

import command.commands.*;
import command.devices.*;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 10:14 PM
 */
public class RemoteControl {

    public enum Mode {ON, OFF}

    private static final int MEMORY_CAPACITY = 5;
    private final Command[] commands;

    public RemoteControl() {
        commands = IntStream.range(0, MEMORY_CAPACITY).mapToObj(slot -> new EmptyCommand()).toArray(Command[] ::new);
    }

    public void setCommand(int slot, Command command) {
        if (slot > MEMORY_CAPACITY || slot < 0)
            throw new IllegalArgumentException("Slot: " + slot + " is out of memory capacity: " + MEMORY_CAPACITY);
        commands[slot] = command;
    }

    public void pushOnButton(int slot) {
        pushButton(slot, Mode.ON);
    }

    public void pushOffButton(int slot) {
        pushButton(slot, Mode.OFF);
    }

    @Override
    public String toString() {
        return "RemoteControl{" +
               "commands=" + Arrays.toString(commands) +
               '}';
    }

    private void pushButton(int slot, Mode mode) {
        System.out.println("Pushed " + mode + " button #" + slot);
        commands[slot].execute(mode);
    }

    public static void main(String[] args) {
        CeilingLightCmd ceilingLightCmd = new CeilingLightCmd(new CeilingLight());
        GardenLightCmd gardenLightCmd = new GardenLightCmd(new GardenLight());
        OutdoorLightCmd outdoorLightCmd = new OutdoorLightCmd(new OutdoorLight());
        MacroCommand gardenAndOutdoorLightCmd = new MacroCommand(Arrays.asList(gardenLightCmd, outdoorLightCmd));
        SecurityCmd securityCmd = new SecurityCmd(new SecurityControl());
        StereoWithCdCmd stereoWithCdCmd = new StereoWithCdCmd(new Stereo());

        RemoteControl remoteControl = new RemoteControl();
        remoteControl.setCommand(2, ceilingLightCmd);
        remoteControl.setCommand(0, gardenAndOutdoorLightCmd);
        remoteControl.setCommand(3, securityCmd);
        remoteControl.setCommand(1, stereoWithCdCmd);

        System.out.println(remoteControl);

        remoteControl.pushOnButton(3);
        remoteControl.pushOnButton(1);
        remoteControl.pushOnButton(2);
        remoteControl.pushOnButton(0);
        remoteControl.pushOffButton(2);
        remoteControl.pushOffButton(1);
        remoteControl.pushOffButton(0);
        remoteControl.pushOffButton(3);
    }
}
