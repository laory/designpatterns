package command.commands;

import command.RemoteControl;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:46 PM
 */
public class MacroCommand implements Command {

    private final List<Command> commands;

    public MacroCommand(List<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void execute(RemoteControl.Mode mode) {
        commands.forEach(command -> command.execute(mode));
    }

    @Override
    public void turnOff() {
        throw new NotImplementedException();
    }

    @Override
    public void turnOn() {
        throw new NotImplementedException();
    }
}
