package command.commands;

import command.devices.CeilingLight;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:59 PM
 */
public class CeilingLightCmd implements Command {


    private final CeilingLight ceilingLight;

    public CeilingLightCmd(CeilingLight ceilingLight) {
        this.ceilingLight = ceilingLight;
    }

    @Override
    public void turnOff() {
        ceilingLight.off();
    }

    @Override
    public void turnOn() {
        ceilingLight.on();
    }
}
