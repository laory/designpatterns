package command.commands;

import command.devices.Stereo;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:59 PM
 */
public class StereoWithCdCmd implements Command {

    private final Stereo stereo;

    public StereoWithCdCmd(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void turnOff() {
        stereo.off();
    }

    @Override
    public void turnOn() {
        stereo.on();
        stereo.setCd(1);
        stereo.setVolume(11);
    }
}
