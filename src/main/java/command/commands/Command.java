package command.commands;

import command.RemoteControl;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:46 PM
 */
public interface Command {

    default void execute(RemoteControl.Mode mode) {
        switch (mode) {
            case ON:
                turnOn();
                break;
            case OFF:
                turnOff();
                break;
            default:
                throw new IllegalArgumentException("Unsupported mode: " + mode);
        }
    }

    void turnOff();

    void turnOn();
}
