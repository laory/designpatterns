package command.commands;

import command.devices.SecurityControl;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:59 PM
 */
public class SecurityCmd implements Command {


    private final SecurityControl securityControl;

    public SecurityCmd(SecurityControl securityControl) {
        this.securityControl = securityControl;
    }

    @Override
    public void turnOff() {
        securityControl.disarm();
    }

    @Override
    public void turnOn() {
        securityControl.arm();
    }
}
