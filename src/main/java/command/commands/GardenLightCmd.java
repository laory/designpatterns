package command.commands;

import command.devices.GardenLight;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:59 PM
 */
public class GardenLightCmd implements Command {

    private final GardenLight gardenLight;

    public GardenLightCmd(GardenLight gardenLight) {
        this.gardenLight = gardenLight;
    }

    @Override
    public void turnOff() {
        gardenLight.manualOff();
    }

    @Override
    public void turnOn() {
        gardenLight.setDawnTime();
        gardenLight.manualOn();
    }
}
