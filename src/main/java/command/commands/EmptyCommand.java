package command.commands;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 10:21 PM
 */
public class EmptyCommand implements Command{

    @Override
    public void turnOff() {

    }

    @Override
    public void turnOn() {

    }
}
