package command.commands;

import command.devices.OutdoorLight;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:59 PM
 */
public class OutdoorLightCmd implements Command {

    private final OutdoorLight outdoorLight;

    public OutdoorLightCmd(OutdoorLight outdoorLight) {
        this.outdoorLight = outdoorLight;
    }

    @Override
    public void turnOff() {
        outdoorLight.off();
    }

    @Override
    public void turnOn() {
        outdoorLight.on();
    }
}
