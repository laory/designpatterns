package command.devices;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:48 PM
 */
public class GardenLight {

    private static final String name = "Garden light";

    public void manualOn() {
        System.out.println(name + " is on");
    }

    public void manualOff() {
        System.out.println(name + " is off");
    }

    public void setDuskTime() {
        System.out.println("Dusk time is set");
    }

    public void setDawnTime() {
        System.out.println("Dawn time is set");
    }
}
