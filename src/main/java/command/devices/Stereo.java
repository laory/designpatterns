package command.devices;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:48 PM
 */
public class Stereo {

    private static final double WAVE = 102.5;

    private static final String name = "Stereo";

    public void on() {
        System.out.println(name + " is on");
    }

    public void off() {
        System.out.println(name + " is off");
    }

    public void setVolume(int vol) {
        System.out.println("Volume is set to " + vol);
    }

    public void setCd(int disc) {
        System.out.println("CD #" + disc + " is set");
    }

    public void setRadio() {
        System.out.println("Radio wave " + WAVE + " is set");
    }
}
