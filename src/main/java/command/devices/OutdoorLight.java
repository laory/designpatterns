package command.devices;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 27/9/16
 * Time: 9:48 PM
 */
public class OutdoorLight {

    private static final String name = "Outdoor light";

    public void on() {
        System.out.println(name + " is on");
    }

    public void off() {
        System.out.println(name + " is off");
    }
}
