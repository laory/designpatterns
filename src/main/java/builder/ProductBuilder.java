package builder;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 11:11 AM
 */
public class ProductBuilder {

    private String  title;
    private double  price;
    private boolean isLimited;

    public ProductBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public ProductBuilder withPrice(double price) {
        this.price = price;
        return this;
    }

    public ProductBuilder withIsLimited(boolean isLimited) {
        this.isLimited = isLimited;
        return this;
    }

    public Product build() {
        return new Product(title, price, isLimited);
    }

    public static void main(String[] args) {
        Product product = new ProductBuilder().withTitle("new product").withPrice(150.5).withIsLimited(true).build();
        System.out.println(product);
    }
}
