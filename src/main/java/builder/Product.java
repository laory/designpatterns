package builder;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/10/2016
 * Time: 11:09 AM
 */
public class Product {

    public final String  title;
    public final double  price;
    public final boolean isLimited;

    public Product(String title, double price, boolean isLimited) {
        this.title = title;
        this.price = price;
        this.isLimited = isLimited;
    }

    @Override
    public String toString() {
        return "Product{" +
               "title='" + title + '\'' +
               ", price=" + price +
               ", isLimited=" + isLimited +
               '}';
    }
}
